Gerua is a responsive, feature-rich Drupal 7 theme. 
It is perfect for any kind of site that requires mobile support along with a site that looks elegant, professional and awesome and which can be used for any business, company, portfolio site.
The fully responsive feature helps the users to browse your site on on both computers and mobile  devices with great ease which helps you to retain potential clients.
This theme is not dependent on any other theme not even any core theme. 
Its very light weight for fast loading with modern look.
It comes with a user interface that allows editing the responsive layout through the theme settings form making it possible for users of all background to create and adjust a fully responsive theme to their needs. 
This theme has a fully responsive layout(s) and highly configurable presentation of that layout in each responsive breakpoint.


Features


- Simple and clean design
- Drupal standards compliant
- CSS based Multi-level drop-down menus
- Adding menus and sub-menus to any level can be achieved.
- Custom and configurable JS Slideshow
 -Minimal design and nice typography
-Supported standard theme features: site name, logo, slider images, social icons, favicon
-Tested on browsers: Safari, Chrome, Opera, Firefox, IE . 